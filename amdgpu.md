# Enable AMDGPU (and use Vulkan), mid-2015 rMPB 15"

Tech specs for [MacBook Pro (Retina, 15-inch, Mid 2015)](https://support.apple.com/kb/SP719):
* MacBook Pro (Retina, 15-inch, Mid 2015)
* Model Identifier: MacBookPro11,4
* Part Number: MJLQ2xx/A
* CPU: Intel Core i7-4870HQ @ 2.50GHz
* GPU: AMD Radeon R9 M370X
* RAM: 16 GB

R9 M370X is (apparantly) based on the Cape Verde (R7 250, GCN 1.0).

____

## Option 1: Updating GRUB to load amdgpu instead of radeon drivers

## Edit GRUB config
You can add the parameters either via a logged in session, of directly from the GRUB cli.

I got this to work, sort of, on Kubuntu 19.04 through 21.10 (22.12.2021), but on KDE Neon it was a complete bork.

### Option 1: Edit GRUB from a logged in session:
Enter the following in your terminal:
`sudo nano /etc/default/grub`

### Option 2: Edit GRUB during boot:
If you want to add the paramters during boot, or if you need to change them due to things not working, you can get to the GRUB CLI by doing the following:

- Press the ESC key *once* during boot. It might take a few tries to time it correctly. The window in which to press Esc is small.
- Select "Advanced options".
- Select the recovery mode kernel image.
- Select root in the menu.

`nano /etc/default/grub`


## Adding parameters

Once you´ve opened the GRUB config file, either logged in or via GRUB CLI, you must add the correct paramters.

Open GRUB config in nano:
`sudo nano etc/default/grub`

### GPU codes for GRUB:

Caribbean Islands GPUs:
`radeon.cik_support=0 amdgpu.cik_support=1`

Sea Islands GPUs:
`radeon.si_support=0 amdgpu.si_support=1`

### In GRUB:

Change the line `GRUB_CMDLINE_LINUX_DEFAULT=` to
`GRUB_CMDLINE_LINUX_DEFAULT=“radeon.si_support=0 amdgpu.si_support=1 radeon.cik_support=0 amdgpu.cik_support=1”`

Then run `sudo update-grub` to, well, update GRUB.

You can also blacklist the radeon driver `rd.driver.blacklist=radeon quiet`:
The entire line would read as follows:
`GRUB_CMDLINE_LINUX_DEFAULT=“radeon.si_support=0 amdgpu.si_support=1 radeon.cik_support=0 amdgpu.cik_support=1 rd.driver.blacklist=radeon quiet”`

If you want to enable the new Dynamic Power Management and Display Code experimental support, you need to also add the following lines:

`amdgpu.dc=1`

`amdgpu.dpm=1`

According to [Level1Techs](https://forum.level1techs.com/t/vulkan-with-amds-gcn-1-0/131427/30) quotes, another kernel option that might be helpful to add to /etc/default/grub is:
`radeon.modeset=0`

### Commands to check which driver is in use

Apparantly, both the AMDGPU and the Radeon drivers will load, but you will set which one takes overship of the GPU. You can check which kernel driver is in use by using one of the following commands:

```
lspci -nnk | grep -iA2 vga
sudo lshw -c video
lspci -nnk | grep -i vga -A3 | grep 'in use'
glxinfo | grep "OpenGL version"
lspci -knn
```
____



_____


## Option 2: Using kernelstub to edit your boot and kernel parameters to use AMDGPU and Vulkan

If it doesn't work via the normal GRUB entries, I found another solution to enable Vulkan on my Macbook Pro running Linux (Kubuntu 19.10), via a [Level1Techs forum post](https://forum.level1techs.com/t/vulkan-with-amds-gcn-1-0/131427/32).

**WARNING:** My attempt at doing this on KDE Neon, broke my graphics. If could probalby have been salvaged, but I didn´t have the patience to find out how. Just turning the parameters "around" (i.e. set a 1 instead of 0, and vice versa, didn´t work). YMMW.

According to [znaque](https://forum.level1techs.com/t/vulkan-with-amds-gcn-1-0/131427/32) over at the Level1Techs board, loading the modules via kernelstub is necessary if using a distro that doesn't use GRUB.

_____


**The ususal "back up your data in case of shit hitting the fan" disclaimer** ¯\\\_(ツ)\_/¯
  

_____


Use `sudo lshw -c video` to check what driver you're using:


``` 
01 *-display
02 description: VGA compatible controller
03 product: Venus XT [Radeon HD 8870M / R9 M270X/M370X]
04 vendor: Advanced Micro Devices, Inc. [AMD/ATI]
05 physical id: 0
06 bus info: pci@0000:01:00.0
07version: 83
08 width: 64 bits
09 clock: 33MHz
10 capabilities: pm pciexpress msi vga_controller bus_master cap_list rom
11 configuration: driver=amdgpu latency=0
12 resources: irq:56 memory:80000000-8fffffff memory:b0c00000-b0c3ffff ioport:3000(size=256) memory:b0c40000-b0c5ffff
```

Line 11 (next to last) show that I'm currently running amdgpu, where previously it was showing radeon.

  
### How to:

Install [Kernelstub](https://github.com/isantop/kernelstub):
* You can either build it yourself (as detailed on the kernelstub github page), or grab a .deb package from the [release page](https://github.com/isantop/kernelstub/releases).


When running `kernelstub amdgpu.si_support=1`, as per the L1T thread, it will ask for sudo, and path to kernel and initrd image.

  
I found the followng paths
* Kernel image: `/boot/vmlinuz`
* Initrd image: `/boot/initrd.img`


After some trial and error, I found the commands supplied under to work. Copy, paste and run each line, one at a time.

`sudo kernelstub --initrd-path /boot/initrd.img --kernel-path /boot/vmlinuz -a amdgpu.si_support=1`

`sudo kernelstub --initrd-path /boot/initrd.img --kernel-path /boot/vmlinuz -a radeon.si_support=0`

`sudo kernelstub --initrd-path /boot/initrd.img --kernel-path /boot/vmlinuz -a amdgpu.cik_support=1`

`sudo kernelstub --initrd-path /boot/initrd.img --kernel-path /boot/vmlinuz -a radeon.cik_support=0`


**Reboot.**

When I rebooted, the boot crapped out when starting over, giving me a black screen. It might have been just me being impatient, but I just held down the power button, forcing a shutdown, then turning it on again when the Apple-light had died. This behaviour was also present when setting boot paramters "normally" via the GRUB config file, so I guess this is the price to pay for amdgpu 

Run `sudo lshw -c video` again to see if it worked.

